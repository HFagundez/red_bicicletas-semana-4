Curso:
Desarrollo del lado servidor: NodeJS, Express y MongoDB

Entrega - Semana 4

- Utilizo EJS como template engine ya que me resulta mas práctico que PUG.

- Se agregaron al menú de navegación: bicicletas, usuarios y login-logout. 

- No olvidar realizar npm install para cargar los módulos.

- Para correr la aplicación:
    iniciar el servidor de mongo con: mongod
    iniciar la app con: npm run devstart
    configurado para puerto 3030: http://localhost:3030/
    

- Se adjunta al repositorio archivo "Capturas Semana 4.zip" con capturas de requerimientos.

- Se suben archivo de variables de ambiente .env a efectos del funcionamiento local de la aplicación.

- Sitio Online Heroku: https://red-bicicletas-rm.herokuapp.com

- Crear usuario o entrar con: 
                                usuario: jdoe@mail.com password: jdoe


Horacio Fagundez - hfagundez@protonmail.com